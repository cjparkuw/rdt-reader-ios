//
//  Copyright (C) 2019 University of Washington Ubicomp Lab
//  All rights reserved.
//
//  This software may be modified and distributed under the terms
//  of a BSD-style license that can be found in the LICENSE file.
//

#ifndef Rdt_iOS_PrefixHeader_h
#define Rdt_iOS_PrefixHeader_h

#ifdef __cplusplus
#import <opencv2/opencv.hpp>

#endif


#endif /* Rdt_iOS_PrefixHeader_h */
